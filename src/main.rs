#![allow(non_snake_case)]
use physical_constants;
use std::process;
use clap::{command, Arg, ArgAction};
use std::collections::HashMap;
use std::collections::BTreeMap;

//List of pyshical constants
const BASE_TEMP: f64 = 288.15;         // Kelvin, at MSL
const BASE_PRESS: f64 = 101325.0;      // pascals, at MSL
const BASE_DENS: f64 = 1.225;          // kilograms per cubic meter, at MSL
const TROPO_TERM_GRAD: f64 = -6.5e-3;  // Kelvin/meter, for troposphere only
const GRAV: f64 = -1.0 * physical_constants::STANDARD_ACCELERATION_OF_GRAVITY; // -9.80665 m/s^2, assuming the negative direction of the force pointing to the earth center 
const AIR_IDEAL_CONS: f64 = 287.05287; // (Joule/kilogram)/Kelvin, for ideal gas Air 
const STRATO_TEMP: f64 = 216.66;       // Kelvin, isothermal temperature for stratosphere
const STRATO_PRESS:f64 = 22552.0;      // pascals, pressure at transition point between tropo and stratosphere =~ 11,000m
const STRATO_DENS:f64 = 0.3629;        // kilogram/meter, density at transition point between tropo and stratosphere =~ 11,000m
const STRATO_ALTITUDE:f64 = 11000.0;   // meters
const EARTH_RADIUS:f64 = 6356766.0;    // meters
const BASE_VISCOSITY:f64 = 1.7894e-5;  // kilogram/(meter*second), at MSL
const SUTHERLAND_TEMP:f64 = 110.0;     // Kelvin, Sutherland constant
const R_U:f64 = physical_constants::MOLAR_GAS_CONSTANT; // Joule/(mol*Kelvin) Ideal molar gas constant

// Geometric to geopotential calculation method
fn geoAlt(alt: f64) -> f64 {
    if alt < 0.0 {
        eprintln!("You are under the ground!!!");
        process::exit(2);
    } else if alt > 20000.0 {
        eprintln!("You are too high for air properties calculation!");
        process::exit(2);
    } else {
        true;
    };
    let result = alt * (EARTH_RADIUS / (EARTH_RADIUS + alt));
    result
}

// Troposphere temperature calculation method
fn Temp(alt: f64) -> f64 {
    let result = BASE_TEMP + (TROPO_TERM_GRAD * alt);
    result
}

// Troposphere pressure calculation method
fn Press(alt: f64) -> f64 {
    let result = BASE_PRESS * f64::powf((BASE_TEMP + (TROPO_TERM_GRAD * alt)) / BASE_TEMP, GRAV / (AIR_IDEAL_CONS * TROPO_TERM_GRAD));
    result
}

// Troposphere density calculation method
fn Densy(alt: f64) -> f64 {
    let result = BASE_DENS * f64::powf((BASE_TEMP + (TROPO_TERM_GRAD * alt)) / BASE_TEMP, (GRAV / (AIR_IDEAL_CONS * TROPO_TERM_GRAD)) - 1.0);
    result
}

// Constant Troposphere temperature encapsuled as a function
fn stratoTemp(_alt: f64) -> f64 {
    STRATO_TEMP
}

// Low stratosphere pressure calculation method
fn stratoPress(alt: f64) -> f64 {
    let result = STRATO_PRESS * f64::exp((GRAV / (AIR_IDEAL_CONS * STRATO_TEMP)) * (alt - STRATO_ALTITUDE));
    result
}

// Low stratosphere density calculation method
fn stratoDensy(alt: f64) -> f64 {
    let result = STRATO_DENS * f64::exp((GRAV / (AIR_IDEAL_CONS * STRATO_TEMP)) * (alt - STRATO_ALTITUDE));
    result
}

// Temperature based viscosity calculation method
fn Visco(alt: f64) -> f64 {
    let alt_temp = Temp(alt);
    let result = BASE_VISCOSITY * ((BASE_TEMP + SUTHERLAND_TEMP) / (alt_temp + SUTHERLAND_TEMP)) * f64::powf(alt_temp / BASE_TEMP, 1.5);
    result
}

// Constant pressure specific heat for Air calculation method, in polynomial form
fn idealCp(temp: f64) -> f64 {
    let cp = 28.11 + (0.1967e-2 * temp) + (0.4802e-5 * f64::powf(temp, 2.0)) + (-1.966e-9 * f64::powf(temp, 3.0));
    cp
}

// Constant volume specific heat for Air calculation method 
fn idealCv(temp: f64) -> f64 {
    let cp = idealCp(temp);
    let cv = cp - R_U;
    cv
}

// Temperature based local speed of sound
fn SoundSpeed(alt: f64) -> f64 {
    let alt_temp = Temp(alt);
    let gamma = idealCp(alt_temp) / idealCv(alt_temp); // Specific heats ratio
    let c = f64::sqrt(gamma * (Press(alt) / Densy(alt)));
    c
}

fn main() {
    let traces = command!()
        .arg(Arg::new("Height")
            .required(true)
            .value_parser(clap::value_parser!(f64)))
        .arg(Arg::new("Temperature")
            .short('t')
            .long("Temperature")
            .action(ArgAction::SetTrue)
            .help("Perform Temperature calculation"))
        .arg(Arg::new("Pressure")
            .short('p')
            .long("Pressure")
            .action(ArgAction::SetTrue)
            .help("Perform Pressure calculation"))
        .arg(Arg::new("Density")
            .short('d')
            .long("Density")
            .action(ArgAction::SetTrue)
            .help("Perform Density calculation"))
        .arg(Arg::new("Viscosity")
            .short('v')
            .long("Viscosity")
            .action(ArgAction::SetTrue)
            .help("Perform Viscosity calculation"))
        .arg(Arg::new("SoundSpeed")
            .short('s')
            .long("SoundSpeed")
            .action(ArgAction::SetTrue)
            .help("Perform Speed of Sound calculation"))
        .get_matches();

    let altitude = geoAlt(*traces.get_one::<f64>("Height").unwrap());

    let mut tropoAttributes: HashMap<String, fn(f64) -> f64> = HashMap::new();
    tropoAttributes.insert(String::from("Temperature"), Temp);
    tropoAttributes.insert(String::from("Pressure"), Press);
    tropoAttributes.insert(String::from("Density"), Densy);
    tropoAttributes.insert(String::from("Viscosity"), Visco);
    tropoAttributes.insert(String::from("SoundSpeed"), SoundSpeed);

    let mut stratoAttributes: HashMap<String, fn(f64) -> f64> = HashMap::new();
    stratoAttributes.insert(String::from("Temperature"), stratoTemp);
    stratoAttributes.insert(String::from("Pressure"), stratoPress);
    stratoAttributes.insert(String::from("Density"), stratoDensy);
    stratoAttributes.insert(String::from("Viscosity"), Visco);
    stratoAttributes.insert(String::from("SoundSpeed"), SoundSpeed);

    let mut results: BTreeMap<&String, f64> = BTreeMap::new();
    if altitude >= 0.0 && altitude <= 11000.0 {
        for (key, value) in tropoAttributes.iter() {
            if traces.get_flag(key) {
                results.insert(key, value(altitude));
            } else {
                continue
            }
        };
        if results.is_empty() {
            for (key, value) in tropoAttributes.iter() {
                results.insert(key, value(altitude));
            }
        }
    } else {
        for (key, value) in stratoAttributes.iter() {
            if traces.get_flag(key) {
                results.insert(key, value(altitude));
            } else {
                continue
            }
        };
        if results.is_empty() {
            for (key, value) in stratoAttributes.iter() {
                results.insert(key, value(altitude));
            }
        }
    }
    for (key, value) in results.iter() {
        println!("{}: {}", key, value);
    }
}
